﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using BST_TableTools;
using EmailNotifications.classes;
using EmailNotifications.Forms;
using EmailNotifications.Properties;
using Microsoft.AspNet.SignalR.Client;
using ProAlert.Andon.Service.Common;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.nonDbModels;
using timeconversions = EmailNotifications.Tools.timeconversions;
using System.Data;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;

namespace EmailNotifications
{
    public partial class Form1 : Form
    {
        #region <-- Members -->

        private Emails _emails;
        private System.Timers.Timer _timer;
        delegate void SetDataSourceCallBack(object obj);
        private delegate void SetProgressCallBack(object obj);
        private int _counter;
        private string _connString;
        private bool _tryingtoReconnect;
        private HubConnection _hubConn;

        #endregion
        public Form1()
        {

            InitializeComponent();
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 20;
            try
            {
                ClickOnceHelper.AddShortcutToStartupGroup("Bridge Software Technologies", "EmailNotifications");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        #region <- SignalR ->

        private async Task<bool> InitHubAsync()
        {
#if DEBUG
            _hubConn = new HubConnection("http://proalert.hopto.org/");
            //_hubConn = new HubConnection("http://localhost:59611/");

#else
            _hubConn = new HubConnection("http://192.168.1.115/"); 
            //_hubConn = new HubConnection("http://proalert.vistechpa/");
            //_hubConn = new HubConnection("http://patest.vistechpa/");

#endif

            var labelProxy = _hubConn.CreateHubProxy("labelRequest");
            labelProxy.On<int>("instantNotification", async scrapId => await SendScrapAsync(scrapId));
            var oeeHubProxy = _hubConn.CreateHubProxy("oeeSummary");
            oeeHubProxy.On<List<WorkCenterProductionStatus>>("prodErrorWarning", async wcs => await SendProductionErrorNotificationAsync(wcs));

            _hubConn.StateChanged += HubConnOnStateChanged;
            _hubConn.ConnectionSlow += HubConnOnConnectionSlow;
            _hubConn.Closed += HubConnOnClosed;
            _hubConn.Reconnecting += HubConnOnReconnecting;
            _hubConn.Reconnected += HubConnOnReconnected;

            try
            {
                await _hubConn.Start();
                return true;
            }
            catch (Exception ex)
            {
                LogAppEvent($"Hub Event: {ex.Message}");
                return false;
            }

        }
        private void HubConnOnStateChanged(StateChange stateChange)
        {
            var msg = "Old State: " + stateChange.OldState + " New State: " + stateChange.NewState;
            //MessageBox.Show(msg);
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertEvent(new AppEvent
                    {
                        EventDT = DateTime.UtcNow,
                        EventMsg = "Hub Event: " + msg,
                        Source = "Print Engine"
                    });
                    repo.Save();
                }
            }
        }
        private async void HubConnOnClosed()
        {
            //MessageBox.Show("Closed");
            if (!_tryingtoReconnect)
            {
                _tryingtoReconnect = true;
                await InitHubAsync();
            }
        }
        private void HubConnOnConnectionSlow()
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertError(new AppError
                    {
                        ErrorDT = DateTime.UtcNow,
                        ErrorMsg = "Hub connection is slow",
                        Source = "Print Engine"
                    });
                    repo.Save();
                }
            }
        }
        private void HubConnOnReconnected()
        {
            _tryingtoReconnect = false;
        }
        private void HubConnOnReconnecting()
        {
            _tryingtoReconnect = true;
        }
        #endregion

        private async Task SendProductionErrorNotificationAsync(List<WorkCenterProductionStatus> wcs)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var activeWcCount = await repo.GetCountAsync<WorkCenter>(x => x.Active == true);
                    var recipients = new List<string>();

#if DEBUG
            recipients.Add("jmikel@bridgesoftwaretechnologies.com");
#else
                    recipients.AddRange(new[] { "jmikel@bridgesoftwaretechnologies.com", "gkerrigan@reyeshayashi.com" });
#endif

                    if (wcs.Count == activeWcCount)
                    {
                        // Send message to BST and Maintenance Manager. Probably Modbus application needs reset.
                        await SendMessage2Async(recipients, "All work centers are not reporting production. The Modbus application may need to be reset.", "ProAlert: All Work Centers Down");
                        return;
                    }

                    var workCenterIds = wcs.Select(wc => wc.WorkCenterId).ToList();
                    var noProductionWorkCenters = await repo.GetAsync<WorkCenter>(
                        filter: x => workCenterIds.Contains(x.Id),
                        includeProperties: "Product"
                    );

                    var workCenterProductionStatusList = noProductionWorkCenters
                        .Select(wc => new
                        {
                            WorkCenterName = wc.Name,
                            ProductName = wc.Product.Name,
                            TotalMinutes = wcs.First(w => w.WorkCenterId == wc.Id).TotalMinutes.ToString("N1"),
                            Source = wcs.First(w => w.WorkCenterId == wc.Id).Source  // Include the source
                        })
                        .ToList();

                    var dt = ConvertToDataTable(workCenterProductionStatusList);
                    var body = "The following work centers have not recorded production for over 10 minutes:<br/>";
                    body = AddHTMLTableToEmailBody(dt, body, true);
                    recipients.Clear();

#if DEBUG
            recipients.Add("jmikel@bridgesoftwaretechnologies.com");
#else
                    recipients.AddRange(new[] { "jmikel@bridgesoftwaretechnologies.com", "gkerrigan@reyeshayashi.com", "bfrederick@reyeshayashi.com", "vcampos@reyeshayashi.com", "mlopez@reyeshayashi.com", "RChing-Herrera@reyeshayashi.com", "Btorres@reyeshayashi.com" });
#endif

                    await SendMessage2Async(recipients, body, "ProAlert: Work Centers Not Reporting Production");
                }
            }
        }
        public static DataTable ConvertToDataTable<T>(IEnumerable<T> items)
        {
            var dataTable = new DataTable(typeof(T).Name);

            // Get all the properties
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                // Setting column names as Property names
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (var item in items)
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    // Inserting property values to DataTable rows
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        public static string AddHTMLTableToEmailBody(DataTable dt, string Body, bool bTime = false)
        {
            int num = 0;
            Body += "<br/><table style='width: 100%; font-family:arial; font-size:15px;' ><tr>";
            foreach (DataColumn column in dt.Columns)
            {
                // Set a custom header for the 'Source' column
                string header = column.ColumnName;
                if (header == "Source")
                {
                    header = "No Cycle Since"; // Change this to your preferred header name
                }
                Body += $"<th>{header}</th>";
            }
            Body += "</tr>";
            foreach (DataRow row in dt.Rows)
            {
                num++;
                Body = num % 2 == 0 ? Body + "<tr style='color:#284775;background-color:White;'>" : Body + "<tr style='color:#333333;background-color:Aqua;'>";
                foreach (DataColumn column in dt.Columns)
                {
                    string columnValue = row[column.ColumnName].ToString();

                    if (column.ColumnName == "TotalMinutes")
                    {
                        // Explicitly handle TotalMinutes as a string
                        Body = Body + "<td>" + columnValue + "</td>";
                    }
                    else if (EmailTools.IsDate(columnValue))
                    {
                        DateTime dateTime = DateTime.Parse(columnValue);
                        Body = Body + "<td>" + (bTime ? dateTime.ToString("M/dd/yyyy HH:mm:ss") : dateTime.ToString("d")) + "</td>";
                    }
                    else
                    {
                        Body = Body + "<td>" + columnValue + "</td>";
                    }
                }
                Body += "</tr>";
            }
            return Body + "</table>";
        }

        private async Task SendScrapAsync(int scrapId)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var scrap = await repo.GetOneAsync<Scrap>(x => x.Id.Equals(scrapId), includeProperties: "WorkCenter, ScrapType, RejectCode");

                    // Get all email addresses of those who have InstantScrapNotification set to true
                    var recipients = _emails.Get().Where(x => x.InstantScrapNotification ?? false).Select(x => x.RecipientEmail).ToList();

                    if (recipients.Any()) // Ensure there are recipients before sending
                    {
                        var rejectName = scrap.RejectCode?.Name ?? "";
                        var rejectDesc = scrap.RejectCode?.Description ?? "";

                        var subject = $"{scrap.WorkCenter.Name} scraped: {scrap.ScrapCount} for {scrap.ScrapType.Name} -- {rejectName}-{rejectDesc}";
                        var messageBody = subject; // Assuming body is same as subject

                        await SendMessage2Async(recipients, subject, messageBody);
                    }
                }
            }
        }
        //private void SendScrap(int scrapId)
        //{
        //    using (var context = new ProAlertContext())
        //    {
        //        using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
        //        {
        //            var scrap = repo.GetOne<Scrap>(x => x.Id.Equals(scrapId), includeProperties:"WorkCenter, ScrapType, RejectCode");
        //            foreach (var em in _emails.Get().Where(x => x.InstantScrapNotification == true))
        //            {
        //                SendMessage(em, scrap.WorkCenter.Name + " scraped: " + scrap.ScrapCount + " for " + scrap.ScrapType.Name + " -- " + scrap.RejectCode.Name + "-" + scrap.RejectCode.Description, scrap.WorkCenter.Name + " scraped: " + scrap.ScrapCount + " for " + scrap.ScrapType.Name + " -- " + scrap.RejectCode.Name + "-" + scrap.RejectCode.Description);
        //            }
        //        }
        //    }
        //}
        private async void Form1_Load(object sender, EventArgs e)
        {
            if (!await InitHubAsync())
            {
                MessageBox.Show("Could not connect to LabelRequestHub");
                Close();
            }
            _emails = new Emails();
            _emails.Load();
            Text = "ProAlert Email Notifications v. " + Assembly.GetExecutingAssembly().GetName().Version;
            //if (Settings.Default.runatstartup)
            //    btnStart_Click(null, null);
            btnStart_Click(null, null);
        }
        private void RegisterNotification()
        {
            _connString = System.Configuration.ConfigurationManager.ConnectionStrings["ProAlertContext"].ConnectionString;
            SqlDependency.Start(_connString);
            var commandText = @"select [CreatedDate] ,[ModifiedDate] ,[CreatedBy] ,[ModifiedBy] ,[MacId] ,[RowVersion] from dbo.email";
            using (var conn = new SqlConnection(_connString))
            {
                using (var command = new SqlCommand(commandText, conn))
                {
                    conn.Open();
                    var sqlDependency = new SqlDependency(command);
                    sqlDependency.OnChange += new OnChangeEventHandler(sqlDependency_OnChange);
                    using (var reader = command.ExecuteReader())
                    {
                    }
                }
            }
        }
        private void sqlDependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            _emails.Load();
            RegisterNotification();
        }
        public void LogAppEvent(string message)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    repo.InsertEvent(new AppEvent
                    {
                        EventDT = DateTime.UtcNow,
                        EventMsg = message,
                        Source = "Email Notificatoin"
                    });
                    repo.Save();
                }
            }
        }
        private void SetProgress(object obj)
        {
            if (progressBar1.InvokeRequired)
            {
                var d = new SetProgressCallBack(SetProgress);
                Invoke(d, new object[] { obj });
            }
            else
            {
                progressBar1.Value = _counter;
            }
        }

        private async Task SendMessage2Async(List<string> recipients, string body, string subjectline)
        {
            if (body.Length < 10)
                return;

            try
            {
                var fromAddress = new MailAddress("proalert-rha@hotmail.com", "ProAlert Email");
                const string fromPassword = "W3tWr!sts!";
                string subject = subjectline;

                var smtp = new SmtpClient
                {
                    Port = 587,
                    Host = "smtp-mail.outlook.com",
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                using (var message = new MailMessage() { Subject = subject, Body = body, IsBodyHtml = true })
                {
                    message.From = fromAddress;

                    foreach (var em in recipients)
                    {
                        var toAddress = new MailAddress(em);
                        message.To.Add(toAddress);
                    }

#if DEBUG
                    await smtp.SendMailAsync(message);
#else
            await smtp.SendMailAsync(message);
#endif
                }
            }
            catch (Exception ex)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        repo.InsertError(new AppError
                        {
                            ErrorMsg = ex.Message,
                            ErrorDT = DateTime.UtcNow,
                            Source = "ProAlertEmail"
                        });
                        await repo.SaveAsync();
                    }
                }
                LogAppEvent("Unable to send ProAlert Email to multiple recipients.");
            }
        }

        public void SendMessage(Email em, string body, string subjectline)
        {

            if (body.Length < 10)
                return;
            try
            {
                //var fromAddress = new MailAddress("midohioboarder@gmail.com", "ProAlert Email");
                var fromAddress = new MailAddress("proalert-rha@hotmail.com", "ProAlert Email");
                var toAddress = new MailAddress(em.RecipientEmail, em.Recipient);
                if (em.Recipient == null)
                    toAddress = new MailAddress(em.RecipientEmail);
                const string fromPassword = "W3tWr!sts!";
                string subject = subjectline;
                var smtp = new SmtpClient
                {
                    //Host = "smtp.gmail.com",
                    Port = 587,
                    Host = "smtp-mail.outlook.com",
                    //Port = 2525,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    //UseDefaultCredentials = true
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
#if DEBUG
                    //if (em.RecipientEmail.Contains("bridge") || em.RecipientEmail.Contains("tmomail"))
                        smtp.Send(message);

#else
                    smtp.Send(message);

#endif

                }
            }
            catch (Exception ex)
            {
                using (var context = new ProAlertContext())
                {
                    using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                    {
                        repo.InsertError(new AppError
                        {
                            ErrorMsg = ex.Message,
                            ErrorDT = DateTime.UtcNow,
                            Source = "ProAlertEmail"
                        });
                        repo.Save();
                    }
                }
                LogAppEvent("Unable to send ProAlert Email to " + em.RecipientEmail);
            }

        }
        public void ReviewDatabase()
        {
            if (_counter == 20)
                _counter = -1;
            _counter += 1;
            SetProgress(_counter);
            // see if any emails need to be sent.
#if DEBUG
            LogAppEvent("Monitoring the System II");
#endif
            //_proAlertEventLog.WriteEntry(, EventLogEntryType.Information);
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    var emp = repo.GetOne<Employee>(x => x.LogIn.Equals("tzi"));
                    var timezone = emp.TimeZone != null ? emp.TimeZone : "Eastern Standard Time";
                    var tzi = TimeZoneInfo.FindSystemTimeZoneById(timezone);

                    var wcsInPco = repo.Get<WCProductTimeline>(
                        x => x.Current && x.PlannedDt && x.LastDTReason.Equals("Product Change Over"),
                        includeProperties: "ProductWcStatsHistory").ToList();

                    foreach (var item in wcsInPco)
                    {

                        var now = DateTime.UtcNow;
                        var dt = (int)now.Subtract(item.Start).TotalMinutes;
                        foreach (var em in _emails.Get().Where(x => ((x.SetupExceeded + item.ProductWcStatsHistory.SetUpTime) < dt) && (x.WorkCenterID == item.WorkCenterID || x.WorkCenterID == null) && x.SetupExceeded > 0))
                        {
                            var pcoList = new List<PCOMsg>();
                            if (!Scheduled(em)) continue;
                            if (dt == em.Delay || ((dt - em.Delay) % (em.Interval == 0 ? Decimal.MaxValue : em.Interval) == 0) && dt > em.Delay)
                            {
                                pcoList.Add(new PCOMsg
                                {
                                    Started = timeconversions.UTCtoClient(tzi, item.Start),
                                    SetupTime = item.ProductWcStatsHistory.SetUpTime.ToString("N1"),
                                    SetUpExceededBy = (dt - item.ProductWcStatsHistory.SetUpTime).ToString("N1"),
                                    WC_Name = em.WC.Name
                                });
                            }

                            if (pcoList.Count > 0)
                            {
                                var data = EmailTools.ConvertToDataTable(pcoList);
                                var body = "The following have exceeded the thresholds<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(data, body, true);
                                SendMessage(em, body, "ProAlert PCO Warning");
                            }
                        }
                    }
#if DEBUG
                    LogAppEvent("Checking for unresolved calls");
#endif

                    // Any WC Unresolved Calls 
                    foreach (var ed in _emails.Get().Where(x => x.Down && x.WorkCenterID == null))
                    {
                        if (!Scheduled(ed)) continue;
                        Dictionary<Enumerations.CallType, List<CallMsg>> callTypeDict = new Dictionary<Enumerations.CallType, List<CallMsg>>();

                        foreach (Enumerations.CallType callType in Enum.GetValues(typeof(Enumerations.CallType))) callTypeDict.Add(callType, new List<CallMsg>());

                        foreach (var c in repo.Get<CallLog>(x => x.Call.DT && x.ResolveDt == null, includeProperties: "Call"))
                        {
#if DEBUG
        LogAppEvent("found unresolved call");
#endif
                            var dt = (int)DateTime.UtcNow.Subtract(c.InitiateDt).TotalMinutes;
                            var curr = repo.GetFirst<WCProductTimeline>(x => x.WorkCenterID == c.WorkCenterId && x.Current);
                            if (curr == null) continue;
                            if (dt == ed.Delay || ((dt - ed.Delay) % (ed.Interval == 0 ? 1 : ed.Interval) == 0) && dt > ed.Delay)
                            {
                                var callMsg = new CallMsg
                                {
                                    WC_Name_Product = curr.WCProduct,
                                    Call_Name = c.Call.Name,
                                    Initiate_DT = timeconversions.UTCtoClient(tzi, c.InitiateDt),
                                    Response_DT = c.ResponseDt == null ? "" : timeconversions.UTCtoClient(tzi, c.ResponseDt ?? DateTime.MinValue).ToLongTimeString(),
                                    Minutes_Down = dt
                                };

                                callTypeDict[c.Call.Type ?? 0].Add(callMsg);
                            }
                        }
                        foreach (Enumerations.CallType callType in Enum.GetValues(typeof(Enumerations.CallType)))
                        {
                            if (callTypeDict[callType].Count > 0)
                            {
                                var dt = EmailTools.ConvertToDataTable(callTypeDict[callType]);
                                var body = "The following work centers are down for " + callType.ToString() + "<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                                // in the .net 6 version of this, we will have departments that users are associated with.  CallType will become a "Department".  We will then send emails to departments.
                                if (ed.RecipientEmail.StartsWith("gkerrigan"))
                                {
                                    if (callType == Enumerations.CallType.Maintenance)
                                        SendMessage(ed, body, "ProAlert DT Calls - " + callType.ToString());
                                }
                                else
                                {
                                    SendMessage(ed, body, "ProAlert DT Calls - " + callType.ToString());
                                }
                            }
                        }
                    }


                    // Specific WC Unresolved Calls
                    foreach (var ed in _emails.Get().Where(x => x.Down && x.WorkCenterID != null))
                    {
                        if (!Scheduled(ed)) continue;
                        var list = new List<CallMsg>();
                        var callTypes = new List<string>();
                        var wcid = ed.WorkCenterID;
                        foreach (var c in repo.Get<CallLog>(x => x.Call.DT && x.ResolveDt == null && x.WorkCenterId == wcid, includeProperties: "Call"))
                        {
                            var dt = (int)DateTime.UtcNow.Subtract(c.InitiateDt).TotalMinutes;
                            var curr = repo.GetFirst<WCProductTimeline>(x => x.WorkCenterID == c.WorkCenterId && x.Current);
                            if (curr == null) continue;
                            if (dt == ed.Delay ||( (dt - ed.Delay) % (ed.Interval == 0 ? Decimal.MaxValue : ed.Interval) == 0) && dt > ed.Delay)
                            {
                                if (Scheduled(ed))
                                {
                                    list.Add(new CallMsg
                                    {
                                        WC_Name_Product = curr.WCProduct,
                                        Call_Name = c.Call.Name,
                                        Initiate_DT = timeconversions.UTCtoClient(tzi, c.InitiateDt),
                                        Response_DT = c.ResponseDt == null ? "" : timeconversions.UTCtoClient(tzi, c.ResponseDt ?? DateTime.MinValue).ToLongTimeString(),
                                        Minutes_Down = dt
                                    });
                                    var name = c.Call.Type == Enumerations.CallType.Maintenance ? "Maintenance" :
                                        c.Call.Type == Enumerations.CallType.Supervisor ? "Quality" : "Materials";

                                    callTypes.Add(name);
                                }

                            }
                        }
                        if (list.Count > 0)
                        {
                            var dt = EmailTools.ConvertToDataTable(list);
                            var body = "The following work centers are down for " + string.Join(", ", callTypes) + "<br/>";
                            body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                            SendMessage(ed, body, "ProAlert DT Calls");
                        }
                    }

                    // Scrap ratio exceeded.
                    foreach (var ed in _emails.Get().Where(x => x.Scrap.Length > 0))
                    {
                        if (!Scheduled(ed)) continue;
                        // parse string and compare scrap numbers for each WC's current time segment
                        var scrapRatio = ed.Scrap.Split(':');
                        var now = DateTime.UtcNow;
                        //var start = now.AddMinutes(-int.Parse(scrapRatio[1]));
                        var list = new List<ScrapMsg>();

                        if (ed.WorkCenterID == null)
                        {
                            foreach (var wc in repo.Get<WorkCenter>())
                            {

                                var wcid = wc.Id;
                                var query = repo.GetOne<WCProductTimeline>(x => x.Current && x.WorkCenterID == wcid, "ProductWCStatsHistory");
                                if (query == null) continue;
                                var unitsPerMin = query.ProductWcStatsHistory.CyclesPerHour / 60 * query.ProductWcStatsHistory.UnitsPerCycle;
                                var startMin = unitsPerMin * int.Parse(scrapRatio[1]);
                                var start = now.AddMinutes((double)-startMin);
                                var cnt = (from s in repo.Get<Scrap>(x => x.WorkCenterID == wcid)
                                           where s.ScrapDT >= start
                                           select s.ScrapCount).DefaultIfEmpty(0).Sum();
                                //ProAlertEventLog.WriteEntry("WC: " + wcid + "  scrap counted: " + cnt + " time: " + start);
                                if (cnt == 0) continue;
                                var ls = (from s in repo.Get<Scrap>(x => x.WorkCenterID == wcid)
                                          where s.ScrapDT >= start
                                          select s.ScrapDT).DefaultIfEmpty(DateTime.MaxValue).Max();
                                var lastScrap = ls == DateTime.MaxValue ? 0 : (int)now.Subtract(ls).TotalMinutes;
                                if (cnt >= int.Parse(scrapRatio[0]))
                                {
                                    list.Add(new ScrapMsg
                                    {
                                        WC_Name = wc.Name,
                                        Scrap_Cnt = cnt,
                                        Last_Scrap = timeconversions.UTCtoClient(tzi, ls)
                                    });
#if DEBUG
                                    LogAppEvent("Scrap ratio exceeded: " + wc.Name + " -- At: " +
                                                            timeconversions.UTCtoClient(tzi, now) + " Scrapped: " + cnt.ToString() +
                                                            " :: TO: " + ed.RecipientEmail + " lastScrap: " + timeconversions.UTCtoClient(tzi, ls));
#endif
                                }

                            }
                            if (list.Count > 0)
                            {
                                var dt = EmailTools.ConvertToDataTable(list);
                                var body = "The following have exceeded the thresholds<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                                SendMessage(ed, body, "ProAlert Scrap Warning");
                            }
                        }
                        else
                        {
                            var wc = repo.GetById<WorkCenter>(ed.WorkCenterID);
                            var wcid = wc.Id;

                            var query = (from wcpdtl in repo.Get<WCProductTimeline>(includeProperties: "ProductWcStatsHistory")
                                         where wcpdtl.Current && wcpdtl.WorkCenterID == wcid
                                         select new { wcpdtl.ProductWcStatsHistory.CyclesPerHour, wcpdtl.ProductWcStatsHistory.UnitsPerCycle }).SingleOrDefault();
                            if (query == null) continue;
                            var unitsPerMin = query.CyclesPerHour / 60 * query.UnitsPerCycle;
                            var startMin = unitsPerMin * int.Parse(scrapRatio[1]);
                            var start = now.AddMinutes((double)-startMin);
                            var cnt = (from s in repo.Get<Scrap>(x => x.WorkCenterID == wcid)
                                       where s.ScrapDT >= start
                                       select s.ScrapCount).DefaultIfEmpty(0).Sum();
                            //ProAlertEventLog.WriteEntry("WC: " + wcid + "  scrap counted: " + cnt + " time: " + start);
                            if (cnt == 0) continue;
                            var ls = (from s in repo.Get<Scrap>(x => x.WorkCenterID == wcid)
                                      where s.ScrapDT >= start
                                      select s.ScrapDT).DefaultIfEmpty(DateTime.MaxValue).Max();
                            var lastScrap = ls == DateTime.MaxValue ? 0 : (int)now.Subtract(ls).TotalMinutes;
                            if (cnt >= int.Parse(scrapRatio[0]))
                            {
                                list.Add(new ScrapMsg
                                {
                                    WC_Name = wc.Name,
                                    Scrap_Cnt = cnt,
                                    Last_Scrap = timeconversions.UTCtoClient(tzi, ls)
                                });
                                // send email.
                                //SendMessage(ed, "Scrap ratio exceeded: " + wc.Name + " -- At (" + timezone + "): " +
                                //                            timeconversions.UTCtoClient(tzi, now) + " Scrapped: " + cnt.ToString() +
                                //                            " :: TO: " + ed.RecipientEmail + " lastScrap: " + timeconversions.UTCtoClient(tzi, ls), "ProAlert Scrap Warning: WC-" + wc.Name);
                                //if (lastScrap % ed.Interval == 0 || ed.Interval == 0)
#if DEBUG
                                LogAppEvent("Scrap ratio exceeded: " + wc.Name + " -- At: " +
                                                        timeconversions.UTCtoClient(tzi, now) + " Scrapped: " + cnt.ToString() +
                                                        " :: TO: " + ed.RecipientEmail + " lastScrap: " + timeconversions.UTCtoClient(tzi, ls));
#endif
                            }
                            if (list.Count > 0)
                            {
                                var dt = EmailTools.ConvertToDataTable(list);
                                var body = "The following have exceeded the thresholds<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                                SendMessage(ed, body, "ProAlert Scrap Warning");
                            }
                        }

                    }
                    // OEE tests

                    foreach (var em in _emails.Get().Where(x => (x.Availability != null || x.Performance != null || x.Quality != null || x.OEE != null)))
                    {
                        if (!Scheduled(em)) continue;

                        var sub = "ProAlert OEE stat warning";
                        var list = new List<OEEMsg>();

                        if (em.WorkCenterID == null)
                        {
                            foreach (var wc in repo.Get<WorkCenter>())
                            {
                                var curr = repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == wc.Id && x.Current);
                                if (curr == null) continue;
                                if (em.Availability != null)
                                {
                                    if (em.Availability > 0)
                                    {
                                        if (em.Availability >= curr.Availability)
                                        {
                                            list.Add(new OEEMsg
                                            {
                                                WC_Name_Product = curr.WCProduct,
                                                Metrics = "Availability",
                                                Value = curr.Availability.ToString("P1"),
                                                Threshold = String.Format("{0:P1}", em.Availability)
                                            });
                                        }
                                    }
                                }
                                if (em.Performance != null)
                                {
                                    if (em.Performance > 0)
                                    {
                                        if (em.Performance >= curr.Performance)
                                        {
                                            list.Add(new OEEMsg
                                            {
                                                WC_Name_Product = curr.WCProduct,
                                                Metrics = "Performance",
                                                Value = curr.Performance.ToString("P1"),
                                                Threshold = String.Format("{0:P1}", em.Performance)
                                            });
                                        }
                                    }
                                }

                                if (em.Quality != null)
                                {
                                    if (em.Quality > 0)
                                    {
                                        if (em.Quality >= curr.Quality)
                                        {
                                            list.Add(new OEEMsg
                                            {
                                                WC_Name_Product = curr.WCProduct,
                                                Metrics = "Quality",
                                                Value = curr.Quality.ToString("P1"),
                                                Threshold = String.Format("{0:P1}", em.Quality)
                                            });
                                        }

                                    }
                                }
                                if (em.OEE != null)
                                {
                                    if (em.OEE > 0)
                                    {
                                        if (em.OEE >= curr.OEE)
                                        {
                                            list.Add(new OEEMsg
                                            {
                                                WC_Name_Product = curr.WCProduct,
                                                Metrics = "OEE",
                                                Value = curr.OEE.ToString("P1"),
                                                Threshold = String.Format("{0:P1}", em.OEE)
                                            });
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            var curr = repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == em.WorkCenterID && x.Current);
                            var wc = repo.GetById<WorkCenter>(em.WorkCenterID);

                            if (curr == null) continue;
                            if (em.Availability != null)
                            {
                                if (em.Availability > 0)
                                {
                                    if (em.Availability >= curr.Availability)
                                    {
                                        list.Add(new OEEMsg
                                        {
                                            WC_Name_Product = curr.WCProduct,
                                            Metrics = "Availability",
                                            Value = curr.Availability.ToString("P1"),
                                            Threshold = $"{em.Availability:P1}"
                                        });
                                    }
                                }
                            }
                            if (em.Performance != null)
                            {
                                if (em.Performance > 0)
                                {
                                    if (em.Performance >= curr.Performance)
                                    {
                                        list.Add(new OEEMsg
                                        {
                                            WC_Name_Product = curr.WCProduct,
                                            Metrics = "Performance",
                                            Value = curr.Performance.ToString("P1"),
                                            Threshold = $"{em.Performance:P1}"
                                        });
                                    }
                                }
                            }

                            if (em.Quality != null)
                            {
                                if (em.Quality > 0)
                                {
                                    if (em.Quality >= curr.Quality)
                                    {
                                        list.Add(new OEEMsg
                                        {
                                            WC_Name_Product = curr.WCProduct,
                                            Metrics = "Quality",
                                            Value = curr.Quality.ToString("P1"),
                                            Threshold = $"{em.Quality:P1}"
                                        });
                                    }

                                }
                            }
                            if (em.OEE != null)
                            {
                                if (em.OEE > 0)
                                {
                                    if (em.OEE >= curr.OEE)
                                    {
                                        list.Add(new OEEMsg
                                        {
                                            WC_Name_Product = curr.WCProduct,
                                            Metrics = "OEE",
                                            Value = curr.OEE.ToString("P1"),
                                            Threshold = $"{em.OEE:P1}"
                                        });
                                    }
                                }
                            }
                        }

                        if (list.Count > 0)
                        {
                            var now = DateTime.UtcNow.Minute;
                            if (em.Interval == 0)
                            {
                                var dt = EmailTools.ConvertToDataTable(list);
                                var body = "The following have exceeded the thresholds<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                                SendMessage(em, body, sub);
#if DEBUG
                                LogAppEvent(sub + " -- " + body);
#endif
                            } else if (now % em.Interval == 0)
                            {
                                var dt = EmailTools.ConvertToDataTable(list);
                                var body = "The following have exceeded the thresholds<br/>";
                                body = EmailTools.AddHTMLTableToEmailBody(dt, body, true);
                                SendMessage(em, body, sub);
#if DEBUG
                                LogAppEvent(sub + " -- " + body);
#endif
                            }
                        }
                    }


                }
            }

        }
        public bool Scheduled(eeEmail ed)
        {
            var found = false;
            var today = DateTime.Today;
            var utcNow = DateTime.UtcNow;
            if (ed.DayTimeFrames.Any(x => x.day == (Enumerations.Day)today.DayOfWeek))
            {
                var dtf = ed.DayTimeFrames.Single(x => x.day == (Enumerations.Day)today.DayOfWeek);
                var tFrom = dtf.From.Value.TimeOfDay;
                var tTo = dtf.To.Value.TimeOfDay;
                if (tFrom > tTo)
                {
                    if ((utcNow.TimeOfDay > tFrom && utcNow < today.AddDays(1)) || (utcNow.TimeOfDay > today.TimeOfDay && utcNow.TimeOfDay < tTo))
                    {
                        found = true;
                    }
                }
                else
                {
                    if (tFrom == tTo) return true;
                    if (utcNow.TimeOfDay >= tFrom && utcNow.TimeOfDay <= tTo)
                        found = true;
                }

            }
            return found;
        }

        public class CallMsg
        {
            public string WC_Name_Product { get; set; }
            public string Call_Name { get; set; }
            public DateTime Initiate_DT { get; set; }
            public string Response_DT { get; set; }
            public int Minutes_Down { get; set; }
        }

        public class ScrapMsg
        {
            public string WC_Name { get; set; }
            public int Scrap_Cnt { get; set; }
            public DateTime Last_Scrap { get; set; }
        }

        public class OEEMsg
        {
            public string WC_Name_Product { get; set; }
            public string Metrics { get; set; }
            public string Value { get; set; }
            public string Threshold { get; set; }
        }

        public class PCOMsg
        {
            public string WC_Name { get; set; }
            public DateTime Started { get; set; }
            public string SetupTime { get; set; }
            public string SetUpExceededBy { get; set; }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new AboutBox1();
            dlg.Show(this);
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            _timer = new System.Timers.Timer {AutoReset = false};
            //_timer.Enabled = true;
            _timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
            _timer.Start();
            RegisterNotification();
        }
        static double GetInterval()
        {
            var now = DateTime.Now;
            return (60 - now.Second) * 1000 - now.Millisecond;
        }
        private void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Interval = GetInterval();
            _timer.Start();
            ReviewDatabase();
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            _timer?.Dispose();
            progressBar1.Value = 0;
            //EnableTextboxes();
            SqlDependency.Stop(_connString);
        }
        private void runAtStartupToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            var mnu = (MenuItem) sender;
            Settings.Default.runatstartup = mnu.Checked;
            Settings.Default.Save();
        }
        private void runAtStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var mnu = (MenuItem)sender;
            Settings.Default.runatstartup = mnu.Checked;
            Settings.Default.Save();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            btnStop_Click(null, null);
            _hubConn.Stop();
        }
        public async Task<string> GetRoleFromWebApp(string username)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://yourmvcwebapp.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync($"api/Users/GetRole/{username}");
                if (response.IsSuccessStatusCode)
                {
                    var role = await response.Content.ReadAsStringAsync();
                    return role;
                }
            }
            return null;
        }

    }
}
