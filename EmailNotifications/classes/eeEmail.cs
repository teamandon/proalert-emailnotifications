﻿using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace EmailNotifications.classes
{
    public class eeEmail : Email
    {
        public WorkCenter WC { get; set; }
    }

    public class Emails
    {
        private readonly List<eeEmail> _list;

        public Emails()
        {
            _list = new List<eeEmail>();
        }

        private void Add(eeEmail e)
        {
            _list.Add(e);
        }

        public void Clear()
        {
            _list.Clear();
        }
        public void Load()
        {
            Clear();

            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {

                    var query = from e in repo.Get<Email>(includeProperties: "WorkCenter, DayTimeFrames")
                        select new eeEmail
                        {
                            WorkCenterID = e.WorkCenterID,
                            Recipient = e.Recipient,
                            RecipientEmail = e.RecipientEmail,
                            Delay = e.Delay,
                            Interval = e.Interval,
                            Down = e.Down,
                            Availability = e.Availability,
                            Performance = e.Performance,
                            Quality = e.Quality,
                            OEE = e.OEE,
                            Scrap = e.Scrap,
                            SetupExceeded = e.SetupExceeded,
                            WC = e.WorkCenter,
                            DayTimeFrames = e.DayTimeFrames.ToList(),
                            InstantScrapNotification = e.InstantScrapNotification
                        };

                    //_list.AddRange(q2);
                    foreach (var e in query)
                    {
                        e.Scrap = e.Scrap == null ? string.Empty : e.Scrap;
                        Add(e);
                    }
                }
            }
        }

        public List<eeEmail> Get()
        {
            return _list;
        }
    }
}
