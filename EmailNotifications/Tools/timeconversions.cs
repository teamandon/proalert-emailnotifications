﻿using System;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace EmailNotifications.Tools
{
    public class timeconversions
    {

        public static DateTime ClientToUTC(TimeZoneInfo tzi, DateTime d)
        {
            var userOffset = tzi.GetUtcOffset(d).TotalHours;
            var webserverOffset = TimeZoneInfo.Local.GetUtcOffset(d).TotalHours;
            var offsetD = d.AddHours(webserverOffset - userOffset);
            offsetD = offsetD.ToUniversalTime();
            return offsetD;
        }

        public static DateTime UTCtoClient(TimeZoneInfo tzi, DateTime utc)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EfReadOnlyRepository.EntityFrameworkReadOnlyRepository<ProAlertContext>(context))
                {
                    if (tzi == null && repo != null)
                    {
                        foreach (var dr in repo.Get<Employee>())
                        {
                            if (dr.GetTimeZoneInstance() != null)
                            {
                                tzi = dr.GetTimeZoneInstance();
                            }
                        }
                        if (tzi == null)
                            tzi = TimeZoneInfo.Local;
                    }
                    else
                    {
                        // use webserver offset
                        tzi = TimeZoneInfo.Local;
                    }
                }
                if (tzi == null || utc == DateTime.MinValue) return DateTime.MinValue;

                var userOffset = tzi.GetUtcOffset(utc).TotalHours;
                var client = utc.AddHours(userOffset);
                return client;
            }
        }
    }
}